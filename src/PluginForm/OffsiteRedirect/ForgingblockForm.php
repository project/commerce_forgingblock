<?php

namespace Drupal\commerce_forgingblock\PluginForm\OffsiteRedirect;

require_once __DIR__ . '/../../Forgingblock/lib/Forgingblock.php';


use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class ForgingblockForm extends PaymentOffsiteForm
{

    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state)
    {
		
        $form = parent::buildConfigurationForm($form, $form_state);

        /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
        $payment = $this->entity;
        /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $paymentGatewayPlugin */
        $paymentGatewayPlugin = $payment->getPaymentGateway()->getPlugin();
				
        $order = $payment->getOrder();				
		
		$amount = $payment->getAmount()->getNumber();
		
		$paymentMachineName = $order->get('payment_gateway')->first()->entity->getOriginalId();
		
		$param =  array('commerce_payment_gateway' => $paymentMachineName);
		$notifyURL = Url::fromRoute('commerce_forgingblock.notify', $param, ['absolute' => TRUE])->toString();						
		
		$trmode = $paymentGatewayPlugin->isTestRequest() ? 'test' : 'live';  
		$forgingblockAPI = new \ForgingblockAPI($trmode);	 	 
	
	  
    	$forgingblockAPI->SetValue('trade', $paymentGatewayPlugin->getTradeID());	 

  
		$forgingblockAPI->SetValue('token', $paymentGatewayPlugin->getToken());
		
		$forgingblockAPI->SetValue('amount', round($amount, 2));								
		
	  
		$forgingblockAPI->SetValue('currency', $payment->getAmount()->getCurrencyCode());		
		
		$forgingblockAPI->SetValue('link', $form['#return_url']);
		
		$forgingblockAPI->SetValue('notification', $notifyURL);
		
		$forgingblockAPI->SetValue('order', $payment->getOrderID());		

	  	
		$resar = $forgingblockAPI->CreateInvoice();
	  
		$Error = $forgingblockAPI->GetError();
		if ($Error) {
	 		//$this->display_error_page(1,$Error);
				echo $Error;
				exit;
			}
			else {
				$payurl	= $forgingblockAPI->GetInvoiceURL();	
				//header("Location: $payurl");
				//exit;
			}
		$rData = array()	;
        return $this->buildRedirectForm($form, $form_state, $payurl, $rData, PaymentOffsiteForm::REDIRECT_GET);
    }
	
	 /**
     * Return notify url
     * {@inheritdoc}
     */
    public function getNotifyUrl($paymentName)
    {
        $url = \Drupal::request()->getSchemeAndHttpHost().'/payment/notify/'.$paymentName;
        return $url;
    }
}
