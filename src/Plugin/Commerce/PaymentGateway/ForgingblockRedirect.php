<?php
namespace Drupal\commerce_forgingblock\Plugin\Commerce\PaymentGateway;

require_once __DIR__ . '/../../../Forgingblock/lib/Forgingblock.php';

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;


/**
 * Provides the forgingblock payment gateway
 * @CommercePaymentGateway(
 *   id = "forgingblock",
 *   label = "Zinari Cryptocurrency Gateway Drupal Commerce",
 *   display_label = "Zinari Cryptocurrency Gateway Drupal Commerce",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_forgingblock\PluginForm\OffsiteRedirect\ForgingblockForm"
 *   }
 * )
 */

class ForgingblockRedirect extends OffsitePaymentGatewayBase
{
	
	/**
     * {@inheritdoc}
     */
    public function defaultConfiguration()
    {
        return [
                'trade_id' => '',
                'token' => '',
            ] + parent::defaultConfiguration();
    }
	
    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state)
    {
        $form = parent::buildConfigurationForm($form, $form_state);
		

        $trade_id = !empty($this->configuration['trade_id']) ? $this->configuration['trade_id'] : '';
        $token = !empty($this->configuration['token']) ? $this->configuration['token'] : '';

        
        
        $form['trade_id'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Trade ID'),
            '#default_value' => $trade_id,
            '#description' => $this->t('Trade Agreement ID.'),
            '#required' => TRUE
        ];

        $form['token'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Token'),
            '#default_value' => $token,
            '#description' => $this->t('Token'),
            '#required' => TRUE
        ];

        
        return $form;
    }


    

    /**
     * {@inheritdoc}
     */
    public function validateConfigurationForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateConfigurationForm($form, $form_state);

        if (!$form_state->getErrors() && $form_state->isSubmitted()) {
            $values = $form_state->getValue($form['#parents']);
            $this->configuration['trade_id'] = $values['trade_id'];
            $this->configuration['token'] = $values['token'];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
    {
        parent::submitConfigurationForm($form, $form_state);
        if (!$form_state->getErrors()) {
            $values = $form_state->getValue($form['#parents']);
            $this->configuration['trade_id'] = $values['trade_id'];
            $this->configuration['token'] = $values['token'];
        }
    }
	/**
     * {@inheritdoc}
     */
    public function onReturn(OrderInterface $order, Request $request)
    {
      
		$payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    	$payment = $payment_storage->create([
      		'state' => 'pending',
      		'amount' => $order->getBalance(),
      		'payment_gateway' => $this->entityId,
      		'order_id' => $order->id(),      
      		'remote_state' => 'pending',
    	]);
    	$payment->save();
    }
	
	/**
     * Notity payment callback
     * @param Request $request
     * @return null|\Symfony\Component\HttpFoundation\Response|void
     */
    public function onNotify(Request $request)
    {
		
		$json_ipn_res = file_get_contents('php://input');		
		$nofify_ar =  json_decode($json_ipn_res, true);
		$invoice_id = $nofify_ar['id'];
		
				
		
		$trmode = $this->isTestRequest() ? 'test' : 'live';  
		$forgingblockAPI = new \ForgingblockAPI($trmode);	 	 
		$forgingblockAPI->SetValue('trade', $this->getTradeID());	   
		$forgingblockAPI->SetValue('token', $this->getToken());
		$forgingblockAPI->SetValue('invoice', $invoice_id);		
		
		$resar = $forgingblockAPI->CheckInvoiceStatus();
		$orderId = $resar['order'];		
		$payment_status = $forgingblockAPI->GetInvoiceStatus();
		
		
        if (!isset($orderId)) {
            \Drupal::messenger()->addMessage($this->t('Site can not get info from you transaction. Please return to store and perform the order'),
                'success');
            $response = new RedirectResponse('/', 302);
            $response->send();
            return;
        }
        
        
        $order = Order::load($orderId);
      
		$newstatus = 'F';
		
		if ($payment_status == 'confirmed') $newstatus = 'P';
		if ($payment_status == 'paid') $newstatus = 'P';		
		if ($payment_status == 'complete') $newstatus = 'P';				
		if ($payment_status == 'expired') $newstatus = 'F';				
       
		    
        if ($newstatus == 'P') {					
							
			$this->update_transaction($order,$resar);
					
          } else {
                    //$this->onCancel($order, $request);
                   return;
          }
            
      
    }
	
	
	/**
     * {@inheritdoc}
     */
  	public function getTradeID() {
    	return $this->configuration['trade_id'];
  	}
  /**
     * {@inheritdoc}
     */
   	public function getToken() {
	   return $this->configuration['token'];
  	}
  /**
     * {@inheritdoc}
     */
  	public function isTestRequest(){
    	return $this->configuration['mode'] == 'test' ? 'TRUE' : 'FALSE';
  	}
	
	public function update_transaction($order, $resdata)
    {
         
		
        $paymentStorage = $this->entityTypeManager->getStorage('commerce_payment');;
        $transactionArray = $paymentStorage->loadByProperties(['order_id' => $order->id()]);
		
		
        if (!empty($transactionArray)) {
            $transaction = array_shift($transactionArray);
        } else {
			
            $transaction = $paymentStorage->create([
                'payment_gateway' => $this->entityId,
                'order_id' => $order->id(),
                'remote_id' => $resdata['id']
            ]);		
        }
		
        //$lastTimeLine = end($charge->timeline);
        $transaction->setRemoteState('complete');
        $transaction->setState('completed');
        $transaction->setAmount($order->getTotalPrice());
        $paymentStorage->save($transaction);
		
    }
	
	
	private function apply_order_transition($order, $orderTransition)
    {
        $order_state = $order->getState();
        $order_state_transitions = $order_state->getTransitions();
        if (!empty($order_state_transitions) && isset($order_state_transitions[$orderTransition])) {
            $order_state->applyTransition($order_state_transitions[$orderTransition]);
            $order->save();
        }
    }
    private function load_order($orderId)
    {
        $order = Order::load($orderId);
        if (!$order) {
            $this->logger->warning(
                'Not found order with id @order_id.',
                ['@order_id' => $orderId]
            );
            throw new BadRequestHttpException();
            return false;
        }
        return $order;
    }
}
